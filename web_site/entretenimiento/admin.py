from django.contrib import admin
#importamos nuestro modelo
from entretenimiento.models import Categoria

from django.db import models

class CategoriaAdmin(admin.ModelAdmin):
    list_display = ("nombre","publicado")

admin.site.register(Categoria, CategoriaAdmin)

