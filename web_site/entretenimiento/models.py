from django.db import models

# Create your models here.

class Categoria(models.Model):
    nombre = models.CharField(max_length=50, verbose_name = 'nombre')
    publicado = models.BooleanField(default = True)
    fech_reg = models.DateTimeField(auto_now_add = True)
    fech_mod = models.DateTimeField(auto_now = True)
    
    def __unicode__(self):
        return self.nombre
