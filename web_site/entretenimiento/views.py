from django.shortcuts import render

# Create your views here.

from django.shortcuts import render_to_response

from entretenimiento.models import Categoria

def inicio(request):
	categorias = Categoria.objects.filter(publicado = True)
	return render_to_response('index.html', {"cat":categorias})
